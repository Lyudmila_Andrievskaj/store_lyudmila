package andrievskay.store.entity;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class Item implements Serializable {


    public Item() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String title;

//    @ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
//    @JoinColumn(name="category_id", referencedColumnName="id")
//
//    @ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
//    @JoinColumn(name="region_id", referencedColumnName="id")

    private int categoryId;
    private long regionId;
    private int brandId;
    private int price;

    private String ownerName;

    private String ownerPhone;
    private String ownerEmail;
    private String description;
    private boolean vip;


//    @ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
//    @JoinColumn(name="brand_id", referencedColumnName="id")


    private Date date;
    private int conditions;

    public Item(String title,
                long region, int categoryId, int price, String ownerName,
                String ownerPhone, String ownerEmail,
                String description, boolean vip,
                int brandId, Date date, int conditions
    ) {

        this.title = title;
//        this.category = category;
        this.regionId = region;
        this.categoryId = categoryId;

        this.price = price;
        this.ownerName = ownerName;
        this.ownerPhone = ownerPhone;
        this.ownerEmail = ownerEmail;
        this.description = description;
        this.vip = vip;
        this.brandId = brandId;
        this.date = date;
        this.conditions = conditions;


//        this.user = user;
    }


//    @ManyToMany(cascade = CascadeType.ALL)
//    @JoinTable(name = "item_user",
//            joinColumns = @JoinColumn(name = "item_id", referencedColumnName="id"),
//            inverseJoinColumns = @JoinColumn(name = "user_id",referencedColumnName="id"))
//    protected Set<User> users;


    public long getRegionId() {
        return regionId;
    }

    public void setRegionId(long regionId) {
        this.regionId = regionId;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerPhone() {
        return ownerPhone;
    }

    public void setOwnerPhone(String ownerPhone) {
        this.ownerPhone = ownerPhone;
    }

    public String getOwnerEmail() {
        return ownerEmail;
    }

    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isVip() {
        return vip;
    }

    public void setVip(boolean vip) {
        this.vip = vip;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getConditions() {
        return conditions;
    }

    public void setConditions(int conditions) {
        this.conditions = conditions;
    }

//    public User getUser() {
//        return user;
//    }
//
//    public void setUser(User user) {
//        this.user = user;
//    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", title='" + title + '\'' +
//                ", category=" + category +
                ", regionId=" + regionId +
                ", price=" + price +
                ", ownerName='" + ownerName + '\'' +
                ", ownerPhone='" + ownerPhone + '\'' +
                ", ownerEmail='" + ownerEmail + '\'' +
                ", description='" + description + '\'' +
                ", vip=" + vip +
                ", brandId=" + brandId +
                ", date=" + date +
                ", conditions=" + conditions +


                '}';
    }
//    public List<DressSize> getDressSizes() {
//        return dressSizes;
//    }
//
//    public void setDressSizes(List<DressSize> dressSizes) {
//        this.dressSizes = dressSizes;
//    }
}
