package andrievskay.store.entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class DressSize {

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private short size;

    public DressSize(short sizeDress, List<Item> item) {
        this.size = sizeDress;

    }

    public DressSize() {
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public short getSizeDress() {
        return size;
    }

    public void setSizeDress(short sizeDress) {
        this.size = sizeDress;
    }
}
