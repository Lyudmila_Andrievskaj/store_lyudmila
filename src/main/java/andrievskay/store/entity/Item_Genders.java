package andrievskay.store.entity;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Item_Genders {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private long itemId;
    private long genderId;

    public Item_Genders() {
    }

    public Item_Genders(long itemId, long gendersId) {
        this.itemId = itemId;
        this.genderId = gendersId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public long getGendersId() {
        return genderId;
    }

    public void setGendersId(long gendersId) {
        this.genderId = gendersId;
    }
}
