package andrievskay.store.entity;
import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


@Entity
public class ShoesSize {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private byte size;



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public byte getShoesSize() {
        return size;
    }

    public void setShoesSize(byte shoesSize) {
        this.size = shoesSize;
    }
}
