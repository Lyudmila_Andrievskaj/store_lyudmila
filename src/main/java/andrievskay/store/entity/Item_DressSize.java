package andrievskay.store.entity;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Item_DressSize {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private long itemId;
    private long dressSizeId;

    public Item_DressSize() {
    }

    public Item_DressSize(long itemId, long dressSizeId) {
        this.itemId = itemId;
        this.dressSizeId = dressSizeId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public long getDressSizeId() {
        return dressSizeId;
    }

    public void setDressSizeId(long dressSizeId) {
        this.dressSizeId = dressSizeId;
    }
}
