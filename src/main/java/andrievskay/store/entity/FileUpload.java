package andrievskay.store.entity;


import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public class FileUpload {

    private List<MultipartFile> photo;

    public List<MultipartFile> getFiles() {
        return photo;
    }

    public void setFiles(List<MultipartFile> photo) {
        this.photo = photo;
    }
}
