package andrievskay.store.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Item_ShoesSize {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long itemId;
    private long shoesSizeId;

    public Item_ShoesSize() {
    }

    public Item_ShoesSize(long itemId, long shoesSizeId) {
        this.itemId = itemId;
        this.shoesSizeId = shoesSizeId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public long getShoes_sizeId() {
        return shoesSizeId;
    }

    public void setShoes_sizeId(long shoes_sizeId) {
        this.shoesSizeId = shoes_sizeId;
    }
}
