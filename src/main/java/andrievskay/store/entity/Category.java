package andrievskay.store.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Category {

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int parentId;
    protected String name;

    public Category() {
    }

    public long getIdCategory() {
        return id;
    }

    public void setIdCategory(int idCategory) {
        this.id = idCategory;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getNameCategory() {
        return name;
    }

    public void setNameCategory(String nameCategory) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", parentId=" + parentId +
                ", name='" + name + '\'' +
                '}';
    }
}
