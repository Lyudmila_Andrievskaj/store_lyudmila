package andrievskay.store.dao;


import andrievskay.store.entity.Region;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface RegionRepository extends JpaRepository<Region, Long> {

    @Modifying
    @Query(value = "SELECT Distinct *from Region",nativeQuery = true)
    List<Region>getRegion();


}
