package andrievskay.store.dao;

import andrievskay.store.entity.Item;
import org.hibernate.SessionFactory;
import org.hibernate.internal.SessionFactoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.LinkedList;
import java.util.List;


public interface ItemRepository extends JpaRepository<Item, Long> {

    @Modifying
    @Query(value = "SELECT *FROM Item where categoryId =?", nativeQuery = true)
    List<Item> findByParentId(int parentId);


//    Item findOne(int id);

    @Modifying
    @Query(value = "SELECT *FROM Item where categoryId =? limit ?, ?", nativeQuery = true)
    List<Item> findById(int parentId, int offset, int limit);

    @Modifying
    @Query(value = "SELECT *FROM Item limit ?, ?", nativeQuery = true)
    List<Item> findAll(int offset, int limit);

    @Modifying
    @Query(value = "SELECT *FROM Item where categoryId =? and regionId= ? and price=? limit ?, ? ", nativeQuery = true)
    List<Item> findByFilter(int parentId, int region, int price, int offset, int limit );

    @Query("SELECT count(id) FROM Item WHERE categoryId =?")
    Integer getCount(int categoryId);



}
