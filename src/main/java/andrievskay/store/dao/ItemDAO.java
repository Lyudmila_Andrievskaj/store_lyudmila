package andrievskay.store.dao;


import andrievskay.store.entity.Item;
import com.springapp.mvc.ItemController;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Repository
public class ItemDAO {


    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

   /*
   find the items by symbol
    */
    @Transactional
    public List findTag(String tag) {
        Query query = null;
        List result;
        String s = "%"+tag+"%";
        query = sessionFactory.getCurrentSession().createQuery("FROM Item where title like ?");
        query.setString(0, s);
        result = query.list();

        return result;

    }
    @Transactional
    public int findByFilterCount(Map map) {
        Query query = null;
        int result = 0;
        int category_id = 0;

        StringBuilder query_builder = null;

        StringBuilder stringBuilder = new StringBuilder("FROM Item where ");
        Iterator iterator = map.entrySet().iterator();
        List<Integer> mas = new ArrayList();


        boolean flag = false;

        while (iterator.hasNext()) {
            Map.Entry mapEntry = (Map.Entry) iterator.next();
            int value = (int) mapEntry.getValue();

            if (flag) {

                if (stringBuilder.lastIndexOf(" AND ") != -1) {
                } else {
                    stringBuilder.append(" AND ");
                }
            }
            if (value == ItemController.ID_ALL_REGION || value == ItemController.ID_ALL_CATEGORY) {

                stringBuilder.append(mapEntry.getKey() + "<>?");

                mas.add(value);
                flag = true;

            } else {
                if ((mapEntry.getKey().equals("price"))) {
                    int price = (int) map.get("price");
                    if ((map.get("price1") == 0)) {
                        int price1 = (int) map.get("price1");
                        if (price != 0 && price1 == 0) {
                            stringBuilder.append("price>=? ");
                            mas.add(value);
                            flag = true;
                        }
                    }
                }
                if ((mapEntry.getKey().equals("price1"))) {
                    int price1 = (int) map.get("price1");
                    if ((map.get("price") == 0)) {
                        int price = (int) map.get("price");
                        if (price1 != 0 && price == 0) {
                            stringBuilder.append("price<=? ");
                            mas.add(value);
                            flag = true;
                        }
                    }
                }
            }
            if ((!mapEntry.getKey().equals("price")) && ((!mapEntry.getKey().equals("price1")))) {
                if (value != ItemController.ID_ALL_REGION && value != ItemController.ID_ALL_CATEGORY) {
                    stringBuilder.append(mapEntry.getKey() + "=?");
                    mas.add(value);
                    flag = true;
                }
            }
        }
        query = sessionFactory.getCurrentSession().createQuery(stringBuilder.toString());
        for (int j = 0; j < mas.size(); j++) {
            query.setInteger(j, mas.get(j));
        }

        result = query.list().size();
        return result;
    }

    @Transactional
    public List findByFilter(Map map, int offset, int limit) {

//        @Query(value = "SELECT *FROM Item where categoryId =? limit ?, ?", nativeQuery = true)

        Query query = null;
        List result = null;
        int category_id = 0;

        StringBuilder query_builder = null;

        StringBuilder stringBuilder = new StringBuilder("FROM Item where ");
        Iterator iterator = map.entrySet().iterator();
        List<Integer> mas = new ArrayList();


        boolean flag = false;

        while (iterator.hasNext()) {
            Map.Entry mapEntry = (Map.Entry) iterator.next();
            int value = (int) mapEntry.getValue();

            if (flag) {

                if (stringBuilder.lastIndexOf(" AND ") != -1) {
                } else {
                    stringBuilder.append(" AND ");
                }
            }

                if (value == ItemController.ID_ALL_REGION || value == ItemController.ID_ALL_CATEGORY) {

                    stringBuilder.append(mapEntry.getKey() + "<>?");

                    mas.add(value);
                    flag = true;

                } else {
                    if ((mapEntry.getKey().equals("price"))) {
                        int price = (int) map.get("price");
                        if ((map.get("price1") == 0)) {
                            int price1 = (int) map.get("price1");
                            if (price != 0 && price1 == 0) {
                                stringBuilder.append("price>=? ");
                                mas.add(value);
                                flag = true;
                            }

                        }
                    }
                    if ((mapEntry.getKey().equals("price1"))) {
                        int price1 = (int) map.get("price1");
                        if ((map.get("price") == 0)) {
                            int price = (int) map.get("price");
                            if (price1 != 0 && price == 0) {
                                stringBuilder.append("price<=? ");
                                mas.add(value);
                                flag = true;
                            }
                        }
                    }

                }
            if ((!mapEntry.getKey().equals("price")) && ((!mapEntry.getKey().equals("price1")))) {
                if (value != ItemController.ID_ALL_REGION && value != ItemController.ID_ALL_CATEGORY) {
                    stringBuilder.append(mapEntry.getKey() + "=?");
                    mas.add(value);
                    flag = true;
                }
            }
//            System.out.println("The key is: " + mapEntry.getKey()
//                    + ",value is :" + mapEntry.getValue());


        }
        query = sessionFactory.getCurrentSession().createQuery(stringBuilder.toString());

        for (int j = 0; j < mas.size(); j++) {
            query.setInteger(j, mas.get(j));
        }

        query.setFirstResult(offset);
        query.setMaxResults(limit);

        result = query.list();

        return result;
    }


    @Transactional
    public void save(Item item) {
        Session session = sessionFactory.getCurrentSession();
        session.save(item);

    }

}
