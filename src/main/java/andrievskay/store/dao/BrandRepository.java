package andrievskay.store.dao;

import andrievskay.store.entity.Brand;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface BrandRepository extends JpaRepository<Brand, Long>{

    @Modifying
    @Query(value = "SELECT Distinct *from Brand",nativeQuery = true)
    List<Brand>getBrand();

    public Brand findByName(String name);
}


