package andrievskay.store.dao;


import andrievskay.store.entity.Category;
import org.jboss.logging.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.persistence.Id;
import java.util.List;

public interface CategoryRepository extends JpaRepository<Category, Long> {
    @Modifying
    @Query(value = "SELECT *FROM Category where parentId =?", nativeQuery = true)
    List<Category> findByParentId(int parentId);

//    @Modifying
//    @Query(value = "SELECT *FROM Category where id =?", nativeQuery = true)
//    public Category findById(int id);
          public Category findById(long id);
}
