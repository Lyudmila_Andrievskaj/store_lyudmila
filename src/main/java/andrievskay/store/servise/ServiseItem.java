package andrievskay.store.servise;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

@Component
public class ServiseItem {


    public static final String PATH_URL_PHOTO = "/resources/Photo_Store/";
    public static final String PATH_URL_PHOTO_ABSOLUTE = "E:\\JAVA\\Работы\\Экзамен\\StoreMVC_25.03\\StoreMVC\\src\\main\\webapp\\resources\\Photo_Store\\";
    private static final int IMG_WIDTH_SMALL = 137;
    private static final int IMG_HEIGHT_SMALL = 91;
    private static final int IMG_WIDTH_BIG = 900;
    private static final int IMG_HEIGHT_BIG = 1000;

    /*
     Check if directory DOES NOT exist crete here
    */
    private boolean checkExistDirectory(long directoryName) {
        File theDir = new File(PATH_URL_PHOTO_ABSOLUTE + directoryName);
        // if the directory does not exist, create it
        if (!theDir.exists()) {
            try {
                theDir.mkdir();
                return true;
            } catch (SecurityException se) {
                se.printStackTrace();
            }

        }
        return false;

    }

    /*
  Save image to file
   */
    public void saveImage(MultipartFile photo, long namePackage, int nameImage) {

        checkExistDirectory(namePackage);

        if (!photo.isEmpty()) {

            try {
                BufferedImage originalImage = ImageIO.read(new ByteArrayInputStream(photo.getBytes()));
               /*
               get size width and height original the photo
                */
                String s = PATH_URL_PHOTO_ABSOLUTE + namePackage + "/" + nameImage + ".jpeg";

                String imageSmall = nameImage + "_small";
                String resizeImage = PATH_URL_PHOTO_ABSOLUTE + namePackage + "/" + imageSmall + ".jpeg";

                File destination = new File(s);

                int type = originalImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : originalImage.getType();

                BufferedImage resizeImageJpgSmall = resizeImageSmall(originalImage, type, IMG_WIDTH_SMALL, IMG_HEIGHT_SMALL);
                ImageIO.write(resizeImageJpgSmall, "jpg", new File(resizeImage));

                ImageIO.write(originalImage, "png", destination);

//                BufferedImage resizeImageJpgBig = resizeImageBig(originalImage, type);
//                ImageIO.write(resizeImageJpgBig, "jpg", new File(resizeImage));


            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /*
    change size image
     */
    private static BufferedImage resizeImageSmall(BufferedImage originalImage, int type, int newWidth, int newHeight) {
        int width = originalImage.getWidth();
        int height = originalImage.getHeight();
        int size[] = getResized(width, height, newWidth, newHeight);

        BufferedImage resizedImageSmall = new BufferedImage(size[0], size[1], type);
        Graphics2D g = resizedImageSmall.createGraphics();
        g.drawImage(originalImage, 0, 0, IMG_WIDTH_SMALL, IMG_HEIGHT_SMALL, null);
        g.dispose();

        return resizedImageSmall;
    }


    /*
   ratio to reduce the size of the image
      */
    private static int[] getResized(int width, int height, int newWidth, int newHeight) {
        int[] arrSize = new int[2];
        if (width > height) {

            float ratio = (float) newWidth / width;
            int thmbHeight = (int) (height * ratio);

            arrSize[0] = newWidth;
            arrSize[1] = thmbHeight;
            return arrSize;


        } else if (height > width) {
            float ratio = (float) newHeight / height;
            int thmbWidth = (int) (width * ratio);

            arrSize[0] = thmbWidth;
            arrSize[1] = newHeight;
            return arrSize;

        } else {

            arrSize[0] = newWidth;
            arrSize[1] = newHeight;
            return arrSize;
        }
    }

    /*
 validate image
*/
    public boolean validateImage(MultipartFile image) {
        if (image.getContentType().equals("image/jpg") || (image.getContentType().equals("image/jpeg")) ||
                (image.getContentType().equals("image/png")) || (image.getContentType().equals("image/png"))) {

            return true;
        }
        return false;
    }

     /*
    get image by id
     */

    public byte[] getImage(String s) throws IOException {

        File file = new File(s);
        FileInputStream fileInputStream = null;
        byte[] bFile = new byte[(int) file.length()];

        //convert file into array of bytes
        fileInputStream = new FileInputStream(file);
        fileInputStream.read(bFile);
        fileInputStream.close();


        return bFile;
    }

    /*
    get path small images
     */
    public ArrayList getFile_Small_Listing(String path) {

        File dir = new File(path);
        File[] files = dir.listFiles();

        ArrayList filPaths = new ArrayList();
        for (File file : files) {

            String file_name = file.getName();
            String buf = file_name.substring(2, 6);
            if (buf.equals("smal")) {

                filPaths.add(file.getName());
            }
        }
        return filPaths;
    }


    public ArrayList getFile_big_listing(String path) {

        File dir = new File(path);
        File[] files = dir.listFiles();

        ArrayList fillBigPaths = new ArrayList();
        for (File file : files) {

            String file_name = file.getName();
            String buf = file_name.substring(2, 6);
            if (!buf.equals("smal")) {

                fillBigPaths.add(file.getName());
            }
        }
        return fillBigPaths;
    }

    /*
    get path  all files in directory by id
     */
    public ArrayList getFileListing(String path) {
        File dir = new File(path);
        File[] files = dir.listFiles();

        ArrayList filPaths = new ArrayList();
        for (File file : files) {
            filPaths.add(file.getName());
        }
        return filPaths;
    }

    /*
    get current date
     */
    public Date getDate() {
        Date today = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd-hh.mm.ss");
        String folderName = formatter.format(today);
        Date convertedDate = null;
        try {
            convertedDate = formatter.parse(folderName);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return convertedDate;
    }
}
