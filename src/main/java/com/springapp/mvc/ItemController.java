package com.springapp.mvc;

import andrievskay.store.dao.*;
import andrievskay.store.entity.*;
import andrievskay.store.servise.ServiseItem;
import com.google.gson.Gson;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.logging.Logger;


@Controller
@RequestMapping("/")
@MultipartConfig
public class ItemController {
    /*
    The table category where parentId = ;
     */
    public static int PARENT_ID_0 = 0;
    public static int PARENT_ID_3 = 3;
    public static int PARENT_ID_5 = 5;
    public static int ID_ALL_CATEGORY = 92;
    public static int ID_ALL_REGION = 26;
    private static final int PAGE_SIZE = 8;


    Gson gson = new Gson();
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private ServiseItem serviseItem;
    @Autowired
    private BrandRepository brandRepository;
    @Autowired
    private RegionRepository regionRepository;
    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    private ItemDAO itemDAO;
//
//    @Autowired
//    private ItemConditionRepository itemConditionRepository;


    @RequestMapping(method = RequestMethod.GET)
    public String startPages(ModelMap model) {
        model.addAttribute("categoryParentIdNull", categoryRepository.findByParentId(PARENT_ID_0));
        model.addAttribute("regions", regionRepository.getRegion());
        Gson gson = new Gson();
        List brandList = itemDAO.findTag("ba");

        return "main";
    }


    @RequestMapping(value = "/itemAddForm", method = RequestMethod.GET)
    public String itemAddForm(ModelMap model) {
        model.addAttribute("regions", regionRepository.getRegion());

        List<Brand> brandList = brandRepository.getBrand();
        model.addAttribute("categoryParentIdNull", categoryRepository.findByParentId(PARENT_ID_0));
        model.addAttribute("categoryParentIdThree", categoryRepository.findByParentId(PARENT_ID_3));
        model.addAttribute("categoryParentId_five", categoryRepository.findByParentId(PARENT_ID_5));
        //rename id->data and name-> value for autocomplete javaScript
        String brandJson = gson.toJson(brandList).replaceAll("id", "data").replaceAll("name", "value");

        model.addAttribute("jsonBrands", brandJson);

        return "itemAddPage";

    }


    /*
    show  items by id with pagination
     */
    @RequestMapping(value = "product-items", method = RequestMethod.GET)
    public String getItemById(ModelMap model, HttpServletRequest request) {
        int page = 1;

        int regionIdFilter = 0;
        int priceFilter = 0;
        int price_1_filter = 0;
        int countByID = 0;
        int categoryId = 0;
        /*
        Колличество старниц
         */
        int noOfPages = 0;
        HashMap filter_map = new HashMap();
        List<Item> itemList = new ArrayList<>();

        if ((request.getParameterValues("quesID")) != null) {
            if (!(request.getParameter("quesID").equals(""))) {
                categoryId = Integer.parseInt(request.getParameter("quesID"));
                if (categoryId != 0) {
                    filter_map.put("categoryId", categoryId);
                }

            }
        }
        if (request.getParameterValues("region") != null) {
            regionIdFilter = Integer.parseInt(request.getParameterValues("region")[0]);
//           regionIdFilter = regionRepository.getRegion().indexOf(0);
            if (regionIdFilter != 0) {
                filter_map.put("regionId", regionIdFilter);
            }
        }
        if ((request.getParameter("price_filter") != null)) {
            if (!(request.getParameter("price_filter").equals(""))) {
                priceFilter = Integer.parseInt(request.getParameter("price_filter"));
            } else {
                priceFilter = 0;
            }
            filter_map.put("price", priceFilter);
        }
        if ((request.getParameter("price1_filter") != null)) {
            if (!(request.getParameter("price1_filter").equals(""))) {
                price_1_filter = Integer.parseInt(request.getParameter("price1_filter"));
            } else {
                price_1_filter = 0;
            }
            filter_map.put("price1", price_1_filter);
        }


        if (request.getParameter("page") != null)
            page = Integer.parseInt(request.getParameter("page"));

        itemList = itemDAO.findByFilter(filter_map, (page - 1) * PAGE_SIZE, PAGE_SIZE);
        countByID = itemDAO.findByFilterCount(filter_map);
        noOfPages = (int) Math.ceil(countByID * 1.0 / PAGE_SIZE);

        model.addAttribute("categoryParentIdNull", categoryRepository.findByParentId(PARENT_ID_0));
        model.addAttribute("itemsById", itemRepository.findByParentId(categoryId));
        model.addAttribute("countPages", countByID / PAGE_SIZE);
        model.addAttribute("itemList", itemList);
        model.addAttribute("noOfPages", noOfPages);
        model.addAttribute("currentPage", page);
        model.addAttribute("categoryId", categoryId);
        model.addAttribute("regionIdFilter", regionIdFilter);
        model.addAttribute("regions", regionRepository.getRegion());

        return "/product-items";

    }

// add item to bd

    @RequestMapping(value = "/addItem", method = RequestMethod.POST)
    public String addItem(HttpServletRequest request, HttpServletResponse response,
                          @RequestParam("photo") MultipartFile[] photo, ModelMap modelMap

    ) throws IOException {
        String title = request.getParameter("title");
        String descriptions = request.getParameter("depiction");
        int price = Integer.parseInt(request.getParameter("price"));
        String name = request.getParameter("name");
        String phone = request.getParameter("phone");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        int brandId = 0;


        int categoryId = Integer.parseInt((request.getParameterValues("category")[0]));

        int regionId = Integer.parseInt(request.getParameterValues("region")[0]);
        if (request.getParameter("brand_id").equals("")) {
            brandId = 0;
        } else {
            brandId = Integer.parseInt(request.getParameter("brand_id"));
        }
        boolean vip = false;
        int condition = Integer.parseInt(request.getParameter("condition"));
        Item item = new Item(title, regionId, categoryId,
                price, name, phone, email, descriptions,
                vip, brandId, new java.util.Date(), condition);


        itemDAO.save(item);

        boolean error = false;

        for (int i = 0; i < photo.length; i++) {
            if (!error) {
                if (photo[i].getSize() != 0) {
                    error = !serviseItem.validateImage(photo[i]);
                }
            }
        }
        if (!error) {
            for (int i = 0; i < photo.length; i++) {
                serviseItem.saveImage(photo[i], item.getId(), i + 1);
            }
        }
        if (!error) {
            return "redirect:/";
        } else
/*
else wrong format of the image  will return all fields
 */
            modelMap.addAttribute("invalidFile", "Неверный формат изображения. Поддерживаемые форматы: jpg, gif, png");
        modelMap.addAttribute("idCategorySelected", categoryId);
        modelMap.addAttribute("idRegionSelected", regionId);
        modelMap.addAttribute("regions", regionRepository.getRegion());
        modelMap.addAttribute("categoryParentIdNulls", categoryRepository.findByParentId(0));
        modelMap.addAttribute("titleSelected", title);
        modelMap.addAttribute("descriptionSelected", descriptions);
        modelMap.addAttribute("priceSelected", price);
        modelMap.addAttribute("nameSelected", name);
        modelMap.addAttribute("phoneSelected", phone);
        modelMap.addAttribute("emailSelected", email);
        List<Brand> brandList = brandRepository.getBrand();
        modelMap.addAttribute("jsonBrands", gson.toJson(brandList).replaceAll("id", "data").replaceAll("name", "value"));
        modelMap.addAttribute("brandIdSelected", brandId);

        return "itemAddPage";
    }


    /*
    get image from file by id
     */

    @RequestMapping(value = "photo", method = RequestMethod.GET)
    @ResponseBody//если мы отправляем не jsp страницу
    public byte[] getPhotoByIdItem(@RequestParam int id) throws IOException {

        String s = ServiseItem.PATH_URL_PHOTO_ABSOLUTE + "\\" + id + "\\" + "1_small" + ".jpeg";
        String notPhoto = ServiseItem.PATH_URL_PHOTO + 0 + "/" + "photo" + ".jpeg";
        try {
            return serviseItem.getImage(s);
        } catch (FileNotFoundException f) {
            return serviseItem.getImage(notPhoto);
        }

    }

    /*
    show one item
     */
    @RequestMapping(value = "item", method = RequestMethod.GET)
    public String item(@RequestParam int id, ModelMap modelMap, HttpServletRequest request) {

        int regionIdFilter = ID_ALL_REGION;
        int categoryIdFilter = ID_ALL_CATEGORY;
        if (request.getParameter("regionIdFilter") != null) {

            regionIdFilter = Integer.parseInt(request.getParameter("regionIdFilter"));
        }
        if (request.getParameter("categoryIdFilter") != null) {
            if (!(request.getParameter("categoryIdFilter").equals(""))) {

                categoryIdFilter = Integer.parseInt(request.getParameter("categoryIdFilter"));
            }
        }
        String s = ServiseItem.PATH_URL_PHOTO_ABSOLUTE + id;

        ArrayList listPathImages = serviseItem.getFile_Small_Listing(s);
        ArrayList listBigPathImages = serviseItem.getFile_big_listing(s);

        Item item = itemRepository.findOne((long) id);
        modelMap.addAttribute("categoryParentIdNull", categoryRepository.findByParentId(PARENT_ID_0));
        modelMap.addAttribute("items", item);
        modelMap.addAttribute("region", regionRepository.findOne((long) id));
        modelMap.addAttribute("images", listPathImages);
        modelMap.addAttribute("imagesBig", listBigPathImages);
        modelMap.addAttribute("regions", regionRepository.getRegion());
        modelMap.addAttribute("regionIdFilter", regionIdFilter);
        modelMap.addAttribute("categoryIdFilter", categoryIdFilter);


        return "product-item";
    }

    /*
    find the items by symbol
     */
    @RequestMapping(value = "/getTags", method = RequestMethod.GET)
    public
    @ResponseBody
    List<Brand> getTags(@RequestParam String title) {
        List list = itemDAO.findTag(title);
        return list;

    }

}



