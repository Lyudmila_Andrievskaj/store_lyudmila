<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>


<html>
<head>
    <link rel="stylesheet" type="text/css" href="../../resources/css/global.css">
    <link rel="stylesheet" type="text/css" href="../../resources/css/style.css">
    <script src="<c:url value="/resources/js/jquery.1.10.2.min.js" />"></script>
    <script src="<c:url value="/resources/js/jquery.autocomplete.min.js" />"></script>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />

    <title>Детские вещи: купить вещи для детей Украине б/у и новые, доступные цены — Малыш</title>
</head>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<body>
<jsp:include page="topMenu.jsp"/>
<div id="content">
    <jsp:include page="leftMenu.jsp"/>
    <div id="main">

        <div name="header-search-input-text-wrap" class="header-search-input-text-wrap sprite-side">

            <input autocomplete="off" type="text" id="w-input-search" class="header-search-input-text passive" placeholder="Поиск" value="" tabindex="1" >
       </div>

        <img src="/resources/images/photo_new.jpg" alt="" width="682" height="334" border="0" usemap="#Map"/>
        <br/>
        <jsp:include page="mainItems.jsp"/>
    </div>
</div>
<jsp:include page="footer.jsp"/>


<script>

    $(document).ready(function() {

        $('#w-input-search').autocomplete({
            serviceUrl: '${pageContext.request.contextPath}/getTags',
            paramName: "title",
            delimiter: ",",
            transformResult: function(response) {
                return {
                    suggestions: $.map($.parseJSON(response), function(item) {
                        return { value: item.title, data: item.id };
                    })
                };
            },
            onSelect:function(response){
                //debugger;
                window.location = '/item?id='+response.data
//				'/test/'+response.data
            }

        });


    });
</script>


</body>
</html>

