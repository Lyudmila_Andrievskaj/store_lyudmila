<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="../../resources/css/global.css">
    <link rel="stylesheet" type="text/css" href="../../resources/css/style.css">

    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>

    <title>Детские вещи: купить вещи для детей Украине б/у и новые, доступные цены — Малыш</title>
</head>
<body>

<div id="sidebar">
    <img src="/resources/images/title1.gif" alt="" width="233" height="41"/><br/>

    <div id="navigation">
        <div></div>
        <ul>
            <c:forEach items="${categoryParentIdNull}" var="categoryParentIdNull">

                <li>
                    <a href="<c:url value="product-items">
            <c:param name="quesID" value="${categoryParentIdNull.idCategory}"/>
            </c:url>
            ">
                            ${categoryParentIdNull.nameCategory}
                    </a>
                </li>
            </c:forEach>
        </ul>
        <div id=" cart">
            <strong>Shopping cart:</strong> <br/> 0 items
        </div>
    </div>
    <div>
        <img src="/resources/images/title1.gif" alt="" width="233" height="41"/><br/>


        <%--<c:set var="categoryId" value="${categoryId}"/>--%>

        <form action="/product-items"
              method="GET" modelattribute="uploadForm">

            <%--<c:set var="categoryId" value="${categoryId}"/>--%>

            <!-- Price filter -->



            <div class="additional-filter">

                <p>Цена (грн.)</p>
                <input type="text" maxlength="10" id="from_price" name="price_filter" value=""/> - <input type="text"
                                                                                                          maxlength="10"
                                                                                                          id="to_price"
                                                                                                          name="price1_filter"/>
                <span>грн.</span>

                <hr/>
            </div>
            <!-- End of Price filter -->
            </br>
            <!-- Region filter -->
            <h3>Регион</h3>

            <div class="filter-items">

                <ul class="left-nav-categories">
                    <select name="region">


                        <c:forEach items="${regions}" var="region">

                            <option value="${region.id}"

                                    <c:if test="${region.id == regionIdFilter}">
                                        selected="selected"
                                    </c:if>
                                    >
                                    ${region.name}

                            </option>

                        </c:forEach>

                    </select>

                </ul>

            </div>
            <!-- End of region filter -->
            </br>
                <input type="hidden" value="${categoryId}" name="quesID" />
                <input type="hidden" value="${regionIdFilter}" name="region" />
            <input class="brown-btn" height="32px" id="filter_by_price" type="submit" name="Показать"/>
        </form>


        <%--<a href="/filter" id="filter_by_price" class="brown-btn">Показать</a>--%>

        <img src="/resources/images/title2.gif" alt="" width="233" height="41"/><br/>

        <div class="review">
            <a href="#"><img src="/resources/images/pic1.jpg" alt="" width="181" height="161"/></a><br/>
            <a href="#">Product 07</a><br/>

            <p>Dolor sit amet, consetetur sadipscing elitr, seddiam nonumy eirmod tempor. invidunt ut labore et
                dolore magna </p>
            <img src="/resources/images/stars.jpg" alt="" width="118" height="20" class="stars"/>
        </div>
    </div>
</div>

</body>
</html>
