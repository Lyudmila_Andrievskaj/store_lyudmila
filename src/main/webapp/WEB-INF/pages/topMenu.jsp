<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>

<html>
<head>
    <link rel="stylesheet" type="text/css" href="../../resources/css/global.css">
    <link rel="stylesheet" type="text/css" href="../../resources/css/style.css">

    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>

    <title>Детские вещи: купить вещи для детей Украине б/у и новые, доступные цены — Малыш</title>
</head>
<body>
<div>
    <a href="itemAddForm"><img src="/resources/images/add_item1.jpg" width="200" height="123" class="float"
                               alt="setalpm"/></a>

    <div class="topnav">
        <span><strong>Welcome</strong> &nbsp;<a href="#">Log in</a> &nbsp; | &nbsp; <a href="#">Register</a></span>
        <select>
            <option>Type of Product</option>
            <option>Clothing</option>
            <option>Accessories</option>
            <option>Clothing</option>
            <option>Accessories</option>
        </select>
        <span>Language:</span> <a href="#"><img src="/resources/images/flag1.jpg" alt="" width="21" height="13"/></a> <a
            href="#"><img src="/resources/images/flag2.jpg" alt="" width="21" height="13"/></a> <a href="#"><img
            src="/resources/images/flag3.jpg" alt="" width="21" height="13"/></a>
    </div>



</div>
<script>

    <%--var json = JSON.stringify(${jsonBrands}, ["data", "value"])--%>

    $(document).ready(function () {

        // find the items by symbol

        $('#w-input-search').autocomplete({
            serviceUrl: '${pageContext.request.contextPath}/getTags',
            paramName: "tag_name",
            delimiter: ",",
            transformResult: function(response) {

                return {

                    suggestions: $.map($.parseJSON(response), function(item) {

                        return { value: item.brand_name, data: item.id };
                    })

                };

            }

        });


    });

</script>
</body>
</html>
