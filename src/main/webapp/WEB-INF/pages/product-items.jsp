<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="select" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>


<head>
    <title>Baby Shop</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="../../resources/css/formoid-solid-orange.css"/>
    <script type="text/javascript" src="../../resources/js/jquery.1.10.2.min.js"></script>
    <script type="text/javascript" src="../../resources/js/jquery.autocomplete.min.js"></script>
    <script type="text/javascript" src="../../resources/js/index.js"></script>
    <script type="text/javascript" src="../../resources/js/formoid-solid-orange.js"></script>
    <script type="text/javascript" src="../../resources/js/lightbox.min.js"></script>

    <link rel="stylesheet" type="text/css" href="../../resources/css/global.css">
    <link rel="stylesheet" type="text/css" href="../../resources/css/style.css">
    <%--<link rel="stylesheet" type="text/css" href="../../resources/css/global.css">--%>
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

    <title>Детские вещи: купить вещи для детей Украине б/у и новые, доступные цены — Малыш</title>
</head>
<body>
<jsp:include page="topMenu.jsp"/>

<div id="content">
    <jsp:include page="leftMenu.jsp"/>

    <div id="main">
        <img src="/resources/images/photo_new.jpg" alt="" width="682" height="334" border="0" usemap="#Map"/>
        <br/>

        <div class="blurBg-false" align="20px">



            <div class="main">

                <c:forEach items="${itemList}" var="itemList">

                    <div class="product-item green " data-item-id="63048">

                        <div class="product-item-image">

                            <a href="item?id=${itemList.id}&regionIdFilter=${regionIdFilter}&categoryIdFilter=${categoryIdFilter}">

                                <img class="image_url" src="/resources/Photo_Store/${itemList.id}/1_small.jpeg"/>
                                    <%--/resources/images/photo_new.jpg--%>


                            </a>
                        </div>

                        <div class="product-item-price">${itemList.price}<span>грн</span></div>

                        <span class="product-item-date">${itemList.date}</span>

                        <p class="product-item-description">
                            <a href="item?id=${itemList.id}&regionIdFilter=${regionIdFilter}&categoryIdFilter=${categoryIdFilter}">
                                    ${itemList.title} </a>
                        </p>

                    </div>
                </c:forEach>

            </div>

            <!-- Start FormPages-->
            <div class="pagination">
                <c:if test="${currentPage != 1}">
                    <td>

                        <a href='<c:url value="product-items">

            <c:param name="page" value="${currentPage - 1}"/>
            <c:param name="quesID" value="${categoryId}"/>
            <%--<c:param name="regionIdFilter" value="${regionIdFilter}"/>--%>
            </c:url>
            '>
                            Previous
                        </a>
                    </td>
                </c:if>

                <%--For displaying Page numbers.

                <The when condition does not display a link for the current page--%>

                <table border="1" cellpadding="5" cellspacing="5" align="center">
                    <tr>
                        <c:forEach begin="1" end="${noOfPages}" var="i">
                            <c:choose>
                                <c:when test="${currentPage eq i}">
                                    <td>${i}</td>
                                </c:when>
                                <c:otherwise>
                                    <td>
                                        <a href='<c:url value="product-items">
            <c:param name="page" value="${i}"/>

             <c:param name="region" value="${regionIdFilter}"/>
            <c:param name="quesID" value="${categoryId}"/>

            </c:url>
            '>
                                                ${i}
                                        </a>

                                    </td>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </tr>
                </table>

                <%--For displaying Next link --%>
                <c:if test="${currentPage lt noOfPages}">
                    <td>

                        <a href='<c:url value="product-items">

            <c:param name="page" value="${currentPage + 1}"/>
           <c:param name="quesID" value="${categoryId}"/>
            </c:url>
            '>
                            Next
                        </a>
                    </td>
                </c:if>
            </div>

        </div>
    </div>

</div>
<!-- Finish FormPages-->
<%--input div --%>
<jsp:include page="footer.jsp"/>
<script>

    <%--var json = JSON.stringify(${jsonBrands}, ["data", "value"])--%>

    $(document).ready(function () {
        $('#brand-search').autocomplete({
            lookup: ${jsonBrands},

            onSelect: function (brand) {
                $('#brand_id').val(brand.data);
            }
        });

        // clear brand id on empty brand name
        $("#brand-search").blur(function () {
            if (!$(this).val())
                $('#brand_id').val('');
        });


          // find the items by symbol

        $('#w-input-search').autocomplete({
            serviceUrl: '${pageContext.request.contextPath}/getTags',
            paramName: "tag_name",
            delimiter: ",",
            transformResult: function(response) {

                return {

                    suggestions: $.map($.parseJSON(response), function(item) {

                        return { value: item.title, data: item.id };
                    })

                };

            }

        });


    });

</script>
</body>
<html>