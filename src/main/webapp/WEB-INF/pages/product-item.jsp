<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="select" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>


<head>
    <title>Baby Shop</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="../../resources/css/formoid-solid-orange.css"/>
    <link rel="stylesheet" href="../../resources/css/lightbox.css"/>
    <script type="text/javascript" src="../../resources/js/jquery.1.10.2.min.js"></script>
    <script type="text/javascript" src="../../resources/js/lightbox.min.js"></script>
    <script type="text/javascript" src="../../resources/js/jquery.autocomplete.min.js"></script>
    <script type="text/javascript" src="../../resources/js/index.js"></script>
    <script type="text/javascript" src="../../resources/js/formoid-solid-orange.js"></script>


    <link rel="stylesheet" type="text/css" href="../../resources/css/global.css">
    <link rel="stylesheet" type="text/css" href="../../resources/css/style.css">
    <%--<link rel="stylesheet" type="text/css" href="../../resources/css/global.css">--%>
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

    <title>Детские вещи: купить вещи для детей Украине б/у и новые, доступные цены — Малыш</title>
</head>
<body>
<jsp:include page="topMenu.jsp"/>

<div id="content">
    <jsp:include page="leftMenu.jsp"/>
    <div id="main">
        <img src="/resources/images/photo_new.jpg" alt="" width="682" height="334" border="0" usemap="#Map"/>
        <br/>

        <div class="goods-body">
            <c:set var="item" value="${items}"/>
            <%--<c:set var="imageBig" value="${imagesBig}"/>--%>

            <h2>${item.title}</h2>


            <div class="goods-body-image">
                <a href="/resources/Photo_Store/${item.id}/${imagesBig[0]}" id="bigImg" data-lightbox="/resources/Photo_Store/${item.id}/${imagesBig[0]}">
                    <img  class="image_url" src="/resources/Photo_Store/${item.id}/${imagesBig[0]}"/>
                </a>

                <div class="item-view-gallery">


                </div>

            </div>

            <div class="goods-body-description">

                <p>

                <div class="addthis_sharing_toolbox"></div>
                </p>

                <h3>Дата объявления:</h3>

                <h3 class="green">${item.date}</h3>

                <h3>Регион</h3>

                <h3 class="green">${region.name}</h3>


                <h3>Цена:</h3>

                <h3 class="product-item-price">${item.price}<span>грн</span></h3>


                <h3>Тел.: <span class="orange">${item.ownerPhone}</span> ${item.ownerName}</h3>

                <ul class="goods-body-thumbs">


                    <c:forEach var="image" items="${images}">


                        <li>
                            <div>
                                <img id="sml_img" class="image_url" src="/resources/Photo_Store/${item.id}/${image}"/>
                                    <%--<img src="${image}">--%>
                            </div>
                        </li>

                    </c:forEach>


                </ul>

            </div>

            <h3>Описание:</h3>

            <p class="green">${item.description}
            </p>

        </div>
    </div>


    <!-- Finish FormPages-->
    <%--input div --%>
    <jsp:include page="footer.jsp"/>

</body>
<html>