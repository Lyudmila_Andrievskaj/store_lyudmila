<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="select" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>


<html>
<head>
    <title>Baby Shop</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="../../resources/css/formoid-solid-orange.css"/>
    <script type="text/javascript" src="../../resources/js/jquery.1.10.2.min.js"></script>
    <script type="text/javascript" src="../../resources/js/jquery.autocomplete.min.js"></script>
    <script type="text/javascript" src="../../resources/js/index.js"></script>
    <script type="text/javascript" src="../../resources/js/formoid-solid-orange.js"></script>


    <link rel="stylesheet" type="text/css" href="../../resources/css/global.css">
    <link rel="stylesheet" type="text/css" href="../../resources/css/style.css">
    <%--<link rel="stylesheet" type="text/css" href="../../resources/css/global.css">--%>
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

    <title>Детские вещи: купить вещи для детей Украине б/у и новые, доступные цены — Малыш</title>
</head>


<body>

<jsp:include page="topMenu.jsp"/>
<div id="content">
    <jsp:include page="leftMenu.jsp"/>

    <div id="main">
        <img src="/resources/images/photo.jpg" alt="" width="682" height="334" border="0" usemap="#Map"/>
        <br/>

        <div class="blurBg-false" style="background-color:#EBEBEB" align="20px">

            <!-- Start FormPages-->
            <jsp:include page="formPages.jsp"/>

        </div>

    </div>
</div>
</div>

<%--input div --%>
<jsp:include page="footer.jsp"/>
<script>

    <%--var json = JSON.stringify(${jsonBrands}, ["data", "value"])--%>

    $(document).ready(function () {
        $('#brand-search').autocomplete({
            lookup: ${jsonBrands},

            onSelect: function (brand) {
                $('#brand_id').val(brand.data);
            }
        });

        // clear brand id on empty brand name
        $("#brand-search").blur(function () {
            if (!$(this).val())
                $('#brand_id').val('');
        });

    });

</script>
</body>
</html>
