<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="select" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title></title>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
</head>
<body>
<!-- Start Formoid form-->
<form class="formoid-solid-orange" action="/addItem"
      method="POST" modelattribute="uploadForm" enctype="multipart/form-data">

  <%--title--%>
  <div class="title"><h2>Заполните форму</h2></div>
  <div class="element-input">
    <label class="title"><span class="required">*</span>
    </label>

    <div class="item-cont">
      <input class="large" type="text" name="title"
             placeholder="Заголовок объявления" value=""/>
      <span class="icon-place"></span>
    </div>
    <%--descriptions--%>
  </div>
  <div class="element-textarea"><label class="title"><span class="required">*</span></label>

    <div class="item-cont">
      <textarea class="medium" type="text" name="depiction" cols="20" rows="5" required="required"
                placeholder="Описание товара"></textarea><span
            class="icon-place"></span>
    </div>
  </div>
  <%--price--%>
  <div class="element-input"><label class="title"><span class="required">*</span></label>

    <div class="item-cont"><input class="large" type="text" name="price" required="required"
                                  placeholder="Цена (грн)"/><span class="icon-place"></span>
    </div>
  </div>
  <%--photo--%>
  <div class="element-file">

    <label class="title"></label>
    <span style="color: #ff0000">${invalidFile} </span>

    <div class="item-cont">

      <label class="large">
        <div class="button">Добавить фотографию</div>

        <input type="file" class="file_input" name="photo"/>

        <div class="file_text">No file selected</div>
        <span class="icon-place"></span>
      </label></div>
  </div>
  <%--photo2--%>
  <div class="element-file"><label class="title"></label>

    <div class="item-cont"><label class="large">
      <div class="button">Добавить фотографию</div>
      <input type="file" class="file_input" name="photo"/>

      <div class="file_text">No file selected</div>
      <span class="icon-place"></span></label></div>
  </div>
  <%--photo3--%>
  <div class="element-file"><label class="title"></label>

    <div class="item-cont"><label class="large">
      <div class="button">Добавить фотографию</div>
      <input type="file" class="file_input" name="photo"/>

      <div class="file_text">No file selected</div>
      <span class="icon-place"></span></label></div>
  </div>
  <%--photo4--%>
  <div class="element-file"><label class="title"></label>

    <div class="item-cont"><label class="large">
      <div class="button">Добавить фотографию</div>
      <input type="file" class="file_input" name="photo"/>

      <div class="file_text">No file selected</div>
      <span class="icon-place"></span></label></div>
  </div>
  <%--category--%>
  <div class="element-multiple"><label class="title"><span class="required">*</span> <b>Категория
    товара</b></label>

    <%--CATECORY--%>
    <div class="item-cont">
      <div class="large">
        <label>
          <select onchange="idCategory(this)" name="category">

            <c:forEach items="${categoryParentIdNull}" var="category">

              <option value="${category.idCategory}"
                      id=${category.idCategory}

                      <c:if test="${category.idCategory == idCategorySelected}">
                              selected="selected"
              </c:if>
                      >
                  ${category.nameCategory}

              </option>
            </c:forEach>
          </select>
        </label><span class="icon-place"></span></div>
    </div>
  </div>
  <%--end--%>

  <%--checkbox--%>
  <div class="sizes-list" style="display: none">
    <label>
      <input type="checkbox" name="size[]" value="{{size.id}}"/> {{size.size}}
    </label>
  </div>
  <%--end--%>

  <%--radio-condition--%>
  <div class="form-item-condition">
    <label>
      <input type="radio" name="condition" value="1" checked="checked"/> Новая вещь
    </label>
    <label>
      <input type="radio" name="condition" value="2"/> Б/у
    </label>
  </div>

  <%--subcategory parent id = 3 - children's furniture--%>
  <div id="subcategoryParentId_3" style="display: none">
    <div class="element-multiple"><label class="title"><span
            class="required">*</span> <b>Подкатегория
      товара</b></label>

      <div class="item-cont">
        <div class="large">
          <select name="subCategory">
            <c:forEach items="${categoryParentIdThree}" var="category">

              <option value=${category.idCategory}>${category.nameCategory}


              </option>

            </c:forEach>

          </select><span class="icon-place"></span>
        </div>
      </div>
    </div>
  </div>
  <%--end--%>

  <%--subcategory parent id = 5 - children's furniture--%>
  <div id="subcategoryParentId_5" style="display: none">
    <div class="element-multiple"><label class="title"><span
            class="required">*</span> <b>Подкатегория
      товара</b></label>

      <div class="item-cont">
        <div class="large">
          <select name="subCategory">
            <c:forEach items="${categoryParentId_five}" var="category">

              <option value=${category.idCategory}>${category.nameCategory}


              </option>

            </c:forEach>

          </select><span class="icon-place"></span>
        </div>
      </div>
    </div>
  </div>
  <%--end--%>


  <%--brand--%>
  <div class="element-input"><label class="title"></label>

    <div class="item-cont">
      <input id="brand-search" class="large" type="text"
             placeholder="Бренд" value="" name="brand">
      <input type="hidden" id="brand_id" name="brand_id" value=""/>
      <span class="icon-place"></span>
    </div>

  </div>
  <%--end--%>

  <%--region--%>
  <div class="element-multiple"><label class="title"><span class="required">*</span></label>

    <div class="item-cont">
      <div class="large">
        <select name="region">
          <c:forEach items="${regions}" var="region">

            <option value="${region.id}"

                    <c:if test="${region.id == idRegionSelected}">
                      selected="selected"
                    </c:if>
                    >


                ${region.name}


            </option>

          </c:forEach>

        </select><span class="icon-place"></span></div>
    </div>
  </div>
  <%--end--%>
  <%--name--%>
  <div class="element-input"><label class="title"><span class="required">*</span></label>

    <div class="item-cont"><input class="large" type="text" name="name" required="required"
                                  value="${nameSelected}"
                                  placeholder="Имя"/><span class="icon-place"></span>
    </div>
  </div>
  <%--end--%>

  <%--phone--%>
  <div class="element-input"><label class="title"><span class="required">*</span></label>

    <div class="item-cont"><input class="large" type="text" name="phone" required="required"
                                  value="${phoneSelected}" placeholder="Телефон"/><span
            class="icon-place"></span>
    </div>
  </div>
  <%--password--%>
  <div class="element-input"><label class="title"><span class="required">*</span></label>

    <div class="item-cont"><input class="large" type="text" name="password" required="required"
                                  value="${phoneSelected}" placeholder="Пароль"/><span
            class="icon-place"></span>
    </div>
  </div>
  <%--email--%>
  <div class="element-email"><label class="title"><span class="required">*</span></label>

    <div class="item-cont"><input class="large" type="email" name="email" value="${emailSelected}"
                                  placeholder="Email"/><span class="icon-place"></span>
    </div>
  </div>
  <div class="submit"><input type="submit" value="Опубликовать"/></div>
</form>


<!-- Stop Formoid form-->

</body>

</html>
